# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import os
import json
import time
import codecs


def processPost(postUrl):
	postData={}
	r=requests.get(postUrl)
	soup=BeautifulSoup(r.text, "html.parser")
	meta=soup.find_all("meta", property=True)
	for i in meta:
		if i["property"]=="og:title":
			postData["title"]=i["content"]
		if i["property"]=="article:published_time":
			postData["published"]=i["content"]
	
	content=soup.find("div", {"class":"entry-content"})
	#Vaatame, kas postitusel tekstilist sisu on.
	if content==None:
		content=soup.find("div", {"class":"entrytext"})
	#Kui ikka ei ole, tagastame 1 ja läheme edasi.
	if content==None:
		return 1
	
	#Võtame sisust scripti ja muu mittevajaliku jura maha
	
	"""
	for tag in content.find_all('div'):
		if tag != None or tag['class'] !="entry_content": #or tag['class'] !="entrytext":
			tag.decompose()
	"""
	try:
		#Python2 jaoks
		html=unicode(content)
	except:
		#Kuna python3-s unicode() funktsiooni pole, kasutame str()
		html=str(content)		
	
	#print (content)
	content=content.text
	#Kuna ülalolev koodijupp kippus html koodi katki tegema, siis sisu puhul kasutame koledalt jõumeetodit.
	#Javascripti asjad
	content=content[:content.find("(function")]
	#Facebooki Jagamisnupud jms
	content=content[:content.find("Share this:")]
	content=content.strip()
	postData["content"] = content
	postData["html"]=html
	postData["url"]=postUrl

	return postData
	
def getPosts(url):
	#Lõikame urli osast http ja feedi maha ning paneme lõigatu kausta nimeks
	tmp=url.split("/")
	dir=tmp[2]
	dir=os.path.join(".", "scraped", dir)
	if not os.path.exists(dir):
		os.mkdir(dir)
		
	r=requests.get(url)
	feed=r.text
	soup=BeautifulSoup(feed, "xml")
	#Võtame postituste lingid
	items=soup.find_all("item")
	for i in items:
		link=i.find("link").get_text()
		print (link)
		tmp=link.split("/")
		postfile=tmp[-2]+".json"
		postfile=os.path.join(dir, postfile)
		if os.path.exists(postfile):
			continue

		post=processPost(link)
		if post==1:
			continue
		with codecs.open(postfile, "w", encoding="utf-8") as js:
			json.dump(post, js, ensure_ascii=False)
		time.sleep(10)

print ("Started crawling for posts.")

with codecs.open("wordpress.csv", "r", encoding="utf-8") as fin:
	for line in fin:
		line=line.strip()
		getPosts(line)
print ("Crawling finished")