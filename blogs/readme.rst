Väike juhend kasutamiseks
=========================

Tegemist on skriptiga, mis loob eesti blogide korpuse.

Käesoleval hetkel sisaldab kood järgmisi funktsionaalsusi:
- blog.tr.ee-s leiduvate blogide viidete kogumine
- eestiblogid.ee-s leiduvate blogide viidete kogumine
- blogide eksisteerimise kontroll
- blogspot.com-is asuvate blogipostituste hankimine
- wordpress.com-is asuvate blogipostituste hankimine

blog.tr.ee
----------
::

$ scrapy crawl blogtr -o blogtr.json

::


eestiblogid.ee
--------------
python eestiblogid.py



blogide kontroll
----------------
Eesmärk välja filtreerida blogid, mis eksisteerivad ja ei ole ära suunatud, kinnised jms.
Teiseks, leida ainult blogspot ja wordpress domeeniga blogid. 

::

$ python validateBlogs.py validate blogs/blogtr.json ./blogtr.csv
$ python validateBlogs.py validate blogs/eestiblogid.json ./eestiblogid.csv

Kui mõlemast kokku korjata, siis ühendamine käsuga
$ sort blogtr.csv eestiblogid.csv -u > blogs.csv
Filtreerimine
$ python validateBlogs.py blogspot ./blogs.csv ./blogspot.csv
$ python validateBlogs.py wordpress ./blogs.csv ./wordpress.csv

::


blogspoti postitused
--------------------
Võtab sisendiks blogspot.csv faili ja alustab blogide postituste läbikäimist.
Väljundi salvestab kataloogi scraped, kus iga blogidomeen on omaette kataloog ja
iga postitus on omaette json fail.

::

$ scrapy crawl blogspot

::

Postituse faili json kujul:

::

{
    "content": "",
    "html": "",
    "title": "",
    "url": "",
    "published": "YYYY-MM-DDTHH:MM:SS+HH:SS"
}

::

Wordpressist postituste kättesaamine

python wordpress.py
