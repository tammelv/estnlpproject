# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import codecs
import json
from urlparse import urlparse
import os

class BlogsPipeline(object):
    def process_item(self, item, spider):
        
        url = urlparse(item["url"])
        folder = ".".join(reversed(url.netloc.split(".")))
        path_name = "scraped/" + folder
        if not os.path.exists(path_name):
            os.mkdir(path_name)
        
        file_name = item["published"] + ".json"
        output_file = path_name + "/" + file_name
        with codecs.open(output_file, 'w', encoding="utf-8") as fout:
            fout.write(json.dumps(dict(item)))
        return item
