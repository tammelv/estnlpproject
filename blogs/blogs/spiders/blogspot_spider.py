# -*- coding: utf-8 -*-

import scrapy
import time
from blogs.items import BlogsItem, BlogspotPostItem
from pprint import pprint
import sys
import re
import logging
logger = logging.getLogger()
import codecs

class BlogspotSpider(scrapy.Spider):
    name = "blogspot"

    allowed_domains =  ["blogspot.com", "blogspot.com.ee"]
    # Kui tahad üksikut blogi kammida, siis kommenteeri välja start_requests() ja
    # kommenteeri sisse allolev rida
    #start_urls = ["http://birgitv.blogspot.com.ee/"]
   
    def start_requests(self):
        with codecs.open('blogspot.csv', 'r', encoding="utf-8") as fin:
            for line in fin:
                url = line.replace(u"\n", u"")
                if len(url):
                    yield scrapy.Request(str(url), self.parse)
        
    def parse(self, response):
        #print response
        #logger.debug(response.url)
        
        crawled_links = []
        
        if (re.search("blogspot\.com(\.ee)?/($|search)", response.url)):
            #logger.debug("Search leht või avaleht")
            next_link = response.xpath('//a[@class="blog-pager-older-link"]/@href').extract()
            post_links = response.xpath('//div[@itemprop="blogPost"]/h3/a/@href').extract()
            
            # lisame "next page" lingid töötlusesse
            if (len(next_link)):
                next_link = list(set(next_link))
                for n_link in next_link:
                    post_links.append(n_link)
                
            #logger.debug(post_links)
            for link in post_links:
                link = str(link)
                if not link in crawled_links:
                    crawled_links.append(link)
                    yield scrapy.Request(link, self.parse)
        else:
            #logger.debug("Post page")
            for item in response.xpath('//div[@itemprop="blogPost"]'):
                post = BlogspotPostItem()
                post["title"] = item.xpath('h3[@itemprop="name"]/text()').extract()[0]
                post["html"] = u"".join(item.xpath('div[contains(@class, "post-body")]').extract())
                post["content"] = u"".join(item.xpath('div[contains(@class, "post-body")]//text()').extract())
                post["url"] = item.xpath('//a[@class="timestamp-link"]/@href').extract()[0]
                post["published"] = item.xpath('//a[@class="timestamp-link"]/abbr[@class="published"]/@title').extract()[0]
                yield post


