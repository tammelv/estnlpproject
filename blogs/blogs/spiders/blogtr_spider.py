# -*- coding: utf-8 -*-

import scrapy
import time
from blogs.items import BlogsItem
from pprint import pprint
import sys

class BlogtrSpider(scrapy.Spider):
    name = "blogtr"
    allowed_domains = ["blog.tr.ee"]
    start_urls = ["http://blog.tr.ee/pages/static/blogs.jhtml"]
    #start_urls = ["http://blog.tr.ee/pages/static/blogs.jhtml?v=v&current_object=8310"]

    def parse(self, response):
        #print response
        #self.parse_list(response)
        for item in response.xpath('//div[@class="dir-list"]'):
            el = BlogsItem()
            el["feed"] = item.xpath('span/a/@href').extract()
            el["title"] = item.xpath('a/text()').extract()
            el["url"] = item.xpath('a/@href').extract()
            el["description"] = item.xpath('text()').extract()
            yield el
        
        time.sleep(2)
        href = response.css('.page_numbers:last-child a.tekst:last-child::attr("href")')
        
        if (len(href)):
            url = "http://blog.tr.ee" + href.extract()[0]
            print url
            yield scrapy.Request(url, self.parse)

