# -*- coding= UTF-8 -*-
import time
from bs4 import BeautifulSoup
import codecs
import requests
import json
import os
def download(url, param=None, headers={'User-Agent':'Crawler for scientific purposes. Contact gerth.jaanimae@gmail.com'}):
	return requests.get(url, params=param, headers=headers)

def removeNonAscii(s):
	return "".join(i for i in s if ord(i)<128)
print ("Started crawling for blog addresses.")
blogs=[]
#Kuna lehel näidatakse korraga 20 blogi, siis suurendame seda pärast 20 võrra.
count=0
while True:
	page	=download("http://eestiblogid.ee/blogid/taehestiku-jaerjekorras?start="+str(count))
	#Töötleme vastust
	content=page.text
	#Kui blogisid enam ei leidu, on number liiga suureks läinud, seega katkestame
	if "Selles kategoorias pole" in content:
		break
	#Võtame välja tabeli osa, kus on meile oluline info
	content=content[content.find("<table"):content.find("</table>")+8]
	content = BeautifulSoup(content, "html.parser")
	#Lingid blogide infole
	links = content.find_all('a')
	#Hakkame vastavaid linke läbi käima
	for i in links:
		i=str(i)
		link=i[i.find("href=")+6:i.find("\">")]
		link="http://eestiblogid.ee/"+link
		content=download(link)
		content=content.text
		soup=BeautifulSoup(content, "html.parser")
		soup=soup.find("div", {"class":"jsn-article-content"})
		if soup is None:
			continue
		blog=soup.find_all("p")
		title=blog[0].text
		title=title[title.find(": ")+2:]
		if title=="":
			continue
		url=blog[1].text
		url=url[url.find(":")+1:]
		url=url.strip()
		
		url=removeNonAscii(url)
		#Mõnel urlil on / lõpus, mõnel ei ole
		if len(url)==0:
			continue
		if url[-1]!="/":
			url+="/"
		#Puhastame urli igasugusest jurast
		
		url=url.replace("\n", "")
		if "http://" not in url and "https://" not in url:
			url="http://"+url
		print (url)
		description=blog[3].text
		description=description[description.find(": ")+2:]
		#description=description.rstrip()
		if "blogspot" in url:
			feed=url+"feeds/posts/default"
		elif "wordpress" in url:
			feed=url+"feed"
		else:
			time.sleep(10)
			continue
		#Blogi info dict
		blogs.append({})
		blogs[-1]["feed"]=[feed]
		blogs[-1]["url"]=[url]
		blogs[-1]["description"]=[description]
		blogs[-1]["title"]=[title]
		time.sleep(10)
	count=count+20
	#Debugimiseks vaid üks lehekülg
	#break
filepath=os.path.join("blogs", "eestiblogid.json")
with open(filepath, "w") as js:
	json.dump(blogs, js)
print ("Crawling finished!")